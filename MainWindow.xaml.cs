﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace dblFolders
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<DuplicateFolder> globalDuplicateList = new List<DuplicateFolder>();
        BackgroundWorker worker = new BackgroundWorker()
        {
            WorkerReportsProgress = true,
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnBrowseFolder_Click(object sender, RoutedEventArgs e)
        {
            var selectFolderDialog = new CommonOpenFileDialog()
            {
                Title = "Select Folder",
                IsFolderPicker = true,
                InitialDirectory = RootPath.Text,
                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true,
            };

            if (selectFolderDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                RootPath.Text = selectFolderDialog.FileName;
            }
        }

        private void btnSearchFolder_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(RootPath.Text))
            {
                btnSearchFolder.IsEnabled = false;
                lbDuplicateFolder.Items.Clear();

                worker.DoWork += worker_SearchForDuplicates;
                worker.ProgressChanged += worker_SearchForDuplicates_ProgressChanged;
                worker.RunWorkerCompleted += worker_SearchForDuplicates_RunWorkerCompleted;
                worker.RunWorkerAsync(RootPath.Text);
            }
            else
            {
                MessageBox.Show("Entered path does not exist.");
            }
        }

        void worker_SearchForDuplicates(object sender, DoWorkEventArgs e)
        {
            var rootPath = e.Argument as string;
            try
            {
                var dirs = createDuplicateFolderList(FindAllFolders(rootPath));

                if (dirs.Count != 0)
                {
                    e.Result = dirs;
                }
                else
                {
                    e.Result = null;
                }
            }
            catch (Exception d)
            {
                MessageBox.Show($"Error while searching for duplicates: {d}");
            }
        }

        void worker_SearchForDuplicates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        void worker_SearchForDuplicates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                globalDuplicateList.Clear();
                globalDuplicateList.AddRange(e.Result as List<DuplicateFolder>);

                foreach (DuplicateFolder dir in globalDuplicateList)
                {
                    lbDuplicateFolder.Items.Add(dir.FolderName);
                }
            }
            else
            {
                MessageBox.Show("No duplicates found.");
            }
            btnSearchFolder.IsEnabled = true;
        }

        List<string> FindAllFolders(string rootDir)
        {
            var pathsToSearch = new Queue<string>();
            var foundFolders = new List<string>();

            pathsToSearch.Enqueue(rootDir);

            while (pathsToSearch.Count > 0)
            {
                var dir = pathsToSearch.Dequeue();

                try
                {
                    foreach (var subDir in Directory.GetDirectories(dir))
                    {
                        foundFolders.Add(subDir);
                        pathsToSearch.Enqueue(subDir);
                    }

                }
                catch (UnauthorizedAccessException)
                {
                    // skip
                }
                catch (PathTooLongException)
                {
                    // skip
                }
            }
            return foundFolders;
        }

        List<DuplicateFolder> createDuplicateFolderList(List<string> allPaths)
        {
            List<DuplicateFolder> allDuplicates;
            var duplicatesGroup = from path in allPaths
                                  group path by new DirectoryInfo(path).Name into duplicateGroup
                                  where duplicateGroup.Count() > 1
                                  select new DuplicateFolder(duplicateGroup.Key, duplicateGroup.ToList());
            allDuplicates = duplicatesGroup.ToList();
            return allDuplicates;
        }

        private void lbDuplicateFolder_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (lbDuplicateFolder.SelectedItem != null)
            {
                lbDuplicateFolderDetails.Items.Clear();
                DuplicateFolder currentItem = globalDuplicateList.Find(
                    path => path.FolderName == lbDuplicateFolder.SelectedItem.ToString());
                foreach (string folder in currentItem.AllFolderPaths)
                {
                    lbDuplicateFolderDetails.Items.Add(folder);
                }
            }
        }

        private void CopyPath_Click(object sender, RoutedEventArgs e)
        {
            MenuItem entry = sender as MenuItem;
            string path = entry.DataContext.ToString();
            Clipboard.SetText(path);
        }

        private void OpenExplorer_Click(object sender, RoutedEventArgs e)
        {
            MenuItem entry = sender as MenuItem;
            string path = entry.DataContext.ToString();
            Process.Start("explorer", Path.GetDirectoryName(path));
        }

        private void DeleteFolder_Click(object sender, RoutedEventArgs e)
        {
            MenuItem entry = sender as MenuItem;
            string pathForDelete = entry.DataContext.ToString();
            Directory.Delete(pathForDelete, true);

            lbDuplicateFolderDetails.Items.Remove(lbDuplicateFolderDetails.SelectedItem);

            DuplicateFolder currentItem = globalDuplicateList.Find(
                path => path.FolderName == lbDuplicateFolder.SelectedItem.ToString());

            currentItem.AllFolderPaths.Remove(pathForDelete);
        }
    }
}
