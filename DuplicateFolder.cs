﻿using System.Collections.Generic;

namespace dblFolders
{
    internal class DuplicateFolder
    {
        public string FolderName { get; private set; }
        public List<string> AllFolderPaths { get; private set; }

        public DuplicateFolder(string name, List<string> paths)
        {
            FolderName = name;
            AllFolderPaths = new List<string>(paths.Capacity);
            AllFolderPaths.AddRange(paths);
        }
    }
}
